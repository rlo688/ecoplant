const express = require('express');
const app = express.Router();

const dataSample = require('./controllers/dataSample.controller');
const rule = require('./controllers/rule.controller');
const etl = require('./controllers/etl.controller');

/**
 * @api {post} /dataSample	               Create new data Sample
 * @apiParam {SampleType} sampleType       temperature / pressure / volume
 * @apiParam {Number} sampleValue          value of sample.
 * @apiParam {String} timestamp	           (Optional) valid date format string
 * @apiParam {Number} gmt                  (Optional) gmt offset
 */
app.post('/dataSample', dataSample.create);

/**
 * @api {get} /dataSample                  Get latest data sample
 * @queryParam {SampleType} sampleType     (Optional) temperature / pressure / volume
 */
app.get('/dataSample', dataSample.getLatest);

/**
 * @api {get} /dataSample/:id              Get one data sample
 * @pathParam {objectId} sampleId          id of sample to retrieve
 */
app.get('/dataSample/:sampleId', dataSample.getOne);

/**
 * @api {get} /dataSamples                 Get all data samples
 * @queryParam {SampleType} sampleType     (Optional) temperature / pressure / volume
 */
app.get('/dataSamples', dataSample.getAll);

/**
 * @api {delete} /dataSamples              Delete all data samples
 */
app.delete('/dataSamples', dataSample.deleteAll);

/**
 * @api {delete} /dataSample/:id           Delete one data sample
 * @pathParam {objectId} sampleId          id of sample to delete
 */
app.delete('/dataSample/:sampleId', dataSample.deleteOne);

/**
 * @api {put} /dataSample	               Modify an existing data Sample
 * @pathParam {ObjectId} sampleId          ID of sample to modify
 * @apiParam {SampleType} sampleType       temperature / pressure / volume
 * @apiParam {Number} sampleValue          value of sample.
 * @apiParam {String} timestamp	           (Optional) valid date format string
 * @apiParam {Number} gmt                  (Optional) gmt offset
 */
app.put('/dataSample/:sampleId', dataSample.modify);

/**
 * @api {post} /rule     	               Create new rule
 * @apiParam {String} rule expression      Example: (10 < Temperature and Temeprature < 100)
 */
app.post('/rule', rule.create);

/**
 * @api {get} /rule/:id                    Get one rule from db
 * @pathParam {objectId} ruleId            id of rule to retrieve
 */
app.get('/rule/:ruleId', rule.getOne);

/**
 * @api {get} /rules                       Get all rules from db
 */
app.get('/rules', rule.getAll);

/**
 * @api {delete} /rules                    Delete all rules from db
 */
app.delete('/rules', rule.deleteAll);

/**
 * @api {delete} /rule/:id                 Delete one rule from db
 * @pathParam {objectId} ruleId            Id of rule to delete
 */
app.delete('/rule/:ruleId', rule.deleteOne);

/**
 * @api {put} /rule     	               Modify exisitng rule
 * @pathParam {ObjectId} ruleId            Id of rule to modify
 * @apiParam {String} epxression           Example: (10 < Temperature and Temeprature < 100)
 */
app.put('/rule/:ruleId', rule.modify);

/**
 * @api {get} /evaluate/:rule     	       Evaluate rule is true / false with latest samples
 * @pathParam {ObjectId} ruleId            Id of rule to evaluate
 */
app.get('/evaluate/:ruleId', rule.evaluate);

/**
 * @api {get} /etl              	       Time-based aggregations
 * @queryParam {SampleType} sampleType     Temperature / pressure / volume
 * @queryParam {Number} resolution=5       (Optional) How many records to cluster together
 * @queryParam {Number} limit              (Optional) Limit # of records
 * @queryParam {String} startingTime       (Optional) Limit records after startingTime
 */
app.get('/etl', etl.aggregate);

module.exports = app;