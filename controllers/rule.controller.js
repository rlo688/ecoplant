const ObjectId = require('mongodb').ObjectID;
const { Rule } = require('../models/rule.model.js');
const dataSample = require('./dataSample.controller');

module.exports = class ruleController {
	static create(req,res) {
		_createRuleFromBody(req, res, (rule)=>{
			_collection(req).insertOne(rule, (err, r)=> {
				if (err) 
				  return _responseError(res, err)
				return res.json({message: `Rule(${r.insertedId}) saved to DB {${req.body.expression}}`}); 
			});
		})
	}

	static modify(req,res) {
		_createRuleFromBody(req, res, (rule)=>{
			_collection(req).update({"_id":ObjectId(req.params.ruleId)}, rule, (err, r)=> {
				if (err) 
					return _responseError(res, err);
				return res.json({message: `Updated ${r.result.n} Document(s)`}); 
			});
		})
	}

	static getAll(req,res) {
		_collection(req).find({}).toArray((err, records)=>{
			if(err) 
				return res.status(500).send({message: err});
			return res.json({message: records});
		});
	}

	static getOne(req,res) {
		_readRuleFromDb(req, res, (rule)=>{
			return res.json({message: rule});	
		});
	}

	static deleteOne(req,res) {
		_deleteAndResponse(req, res, {"_id":ObjectId(req.params.ruleId)});
	}

	static deleteAll(req,res) {
		_deleteAndResponse(req, res, {});
	}

	static evaluate(req,res) {
		_readRuleFromDb(req, res, (rule)=>{
			let ruleObj ;
			let latestSamples ;
			let result ;
			try {
				ruleObj = Rule.createFromJson(rule) ;
				dataSample.latestSamples(req.app.db.collection('dataSample')).then(latestSamples=>{
					result = ruleObj.evaluate(latestSamples) ;
					return res.json({
						latestSamples: latestSamples,
						rule: rule,
						result: result,
						message: `Result for rule(${rule._id}) is ${result}`
					});
				}).catch(err=>_responseError(res,err)) ;;
			}
			catch(err)
			{
				_responseError(res, err);
			}
		});
	}
}

function _collection(req) {
	return req.app.db.collection('rule') ;
}

function _responseError(res, err) {
	console.log(err);
	return res.status(400).send({message: ""+err});
}

function _readRuleFromDb(req, res, next) {
	const collection = req.app.db.collection('rule');
	collection.findOne({"_id":ObjectId(req.params.ruleId)}).then(rule => {
		if(!rule) 
			return res.status(404).send({message: "Document not found with id " + req.params.ruleId});
		return next(rule);
	}).catch(err => { res.status(500).send({message: "Error retrieving document with id " + req.params.ruleId}) });
}

function _createRuleFromBody(req, res, next) {
		let rule ;
		try {
			rule = new Rule(req.body.expression)
		}
		catch(err) {
			return res.status(400).send({message: ""+err});
		}
		next(rule)
}

function _deleteAndResponse(req, res, query) {
	_collection(req).deleteMany(query)
	.then(obj => res.json({message: `${obj.result.n} document(s) deleted`}))
	.catch(err => _responseError(res, err));
}