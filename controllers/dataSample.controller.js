const ObjectId = require('mongodb').ObjectID;
const { DataSample } = require('../models/dataSample.model.js');

module.exports = class dataSampleController {
	static create(req,res) {
		_createSampleFromBody(req, res, (sample)=>{
			_collection(req).insertOne(sample, (err, r)=> {
				if (err) {
				  return res.status(400).send({message: err});
				}
				return res.json({message: `Sample(${r.insertedId}) saved to DB {${req.body.sampleType},${req.body.sampleValue},${sample._timestamp}}`}); 
			});
		});
	}

	static modify(req,res) {
		_createSampleFromBody(req, res, (sample)=>{
			_collection(req).update({"_id":ObjectId(req.params.sampleId)}, sample, (err, r)=> {
				if (err) {
				  return res.status(400).send({message: err});
				}
				return res.json({message: `Updated ${r.result.n} Document(s)`}); 
			});
		});
	}

	static getLatest(req,res) {
		let query = {} ;
		if(req.query.sampleType)
			query = {"_sampleType": req.query.sampleType};
		_findLatest(_collection(req), query).then(record=>{
			return res.json({message: record});
		}).catch(err=>res.status(400).send({message: err}));
	}

	static getAll(req,res) {
		let query = {} ;
		if(req.query.sampleType)
			query =  {"_sampleType": req.query.sampleType};
		_collection(req).find(query).toArray((err, records)=>{
			if(err) 
				return res.status(500).send({message: err});
			res.json({message: `Found ${records.length} Document(s)`, data: records});
		});
	}

	static getOne(req,res) {
		_collection(req).findOne({"_id":ObjectId(req.params.sampleId)}).then(sample => {
			if(!sample) 
				return res.status(404).send({message: "Data sample not found with id " + req.params.sampleId});
			return res.json({message: sample});
		}).catch(err => { res.status(500).send({message: "Error retrieving sample with id " + req.params.sampleId}) });
	}

	static deleteOne(req,res) {
		_deleteAndResponse(req, res, {"_id":ObjectId(req.params.sampleId)});
	}

	static deleteAll(req,res) {
		_deleteAndResponse(req, res, {});
	}

	static latestSamples(collection) {
		return new Promise((resolve,reject)=>{
			_findLatest(collection, {"_sampleType": "pressure"}).then(pressure=>{
				_findLatest(collection, {"_sampleType": "temperature"}).then(temperature=>{
					_findLatest(collection, {"_sampleType": "volume"}).then(volume=>{
						return resolve({
							"pressure": pressure._sampleValue,
							"temperature": temperature._sampleValue,
							"volume": volume._sampleValue,
						});
					}).catch(reject);
				}).catch(reject);
			}).catch(reject);
		});
	}

}

function _collection(req) {
	return req.app.db.collection('dataSample') ;
}

function _createSampleFromBody(req,res,next) {
	let sample ;
	try {
		sample = new DataSample(req.body.sampleType, req.body.sampleValue, req.body.timestamp, req.body.gmt);
	} 
	catch(err) {
		return res.status(400).send({message: ""+err});
	}
	next(sample) ;
}

function _deleteAndResponse(req, res, query) {
	_collection(req).deleteMany(query)
	.then(obj => res.json({message: `${obj.result.n} document(s) deleted`}))
	.catch(err => res.status(500).send({message: ""+err}));
}

function _findLatest(collection, query) {
	return new Promise((resolve,reject)=>{
		collection.find(query).sort({_timestamp: -1}).limit(1).toArray((err, records)=>{
			if(err)
				throw err ;
			if(records.length<1)
			{
				reject(`Error finding latest sample: No samples for ${query?query._sampleType:'{}'}.`) ;
			}
			resolve(records[0]);
		});
	});
}
