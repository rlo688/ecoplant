const Timestamp = require('../models/timestamp.model.js');

module.exports = class etlController {
	static aggregate(req,res) {
		let results = {
			resolution: undefined,
			totalSamples: undefined,
			sampleType: undefined,
			data: []
		}

		let resolution = 5 ; //default resolution - 5 samples
		let limit = 0 ; //default no limit (all records)
		let query = {_sampleType: req.query.sampleType} ;

		if(!req.query.sampleType)
			return res.status(400).send({message: "Missing 'sampleType' in query params"});

		if(req.query.resolution) {
			resolution = parseInt(req.query.resolution) ;
			if(isNaN(resolution))
				return res.status(400).send({message: "Resolution query param must be a number"});
		}

		if(req.query.limit)	{
			limit = parseInt(req.query.limit) ;
			if(isNaN(limit))
				return res.status(400).send({message: "Limit query param must be a number"});
		}
		
		if(req.query.startingTime) {
			let startingDate ;
			try {
				startingDate = Timestamp(req.query.startingTime, req.query.gmt) ;
			}
			catch(err)
			{
				return res.status(400).send(err);
			}
			query["_timestamp"] = {"$gte": startingDate};
		}

		const collection = req.app.db.collection('dataSample');
		collection.find(query).sort({_timestamp: 1}).limit(limit).toArray((err, records)=>{
			if(err) 
				return res.status(500).send({message: err});

			results.totalSamples = records.length ;
			results.resolution = resolution ;
			results.sampleType = req.query.sampleType ;

			while(records.length>0)
			{
				const _records = records.splice(0,resolution);
				const result = _records.reduce(_minMaxCountReducer, _initReducer());
				results.data.push(result);
			}
			return res.json(results);
		});
	}
}

function _minMaxCountReducer(result, current, index, array) {
	const val = current._sampleValue;

	if(!result.min || val < result.min)
		result.min = val;
	if(!result.max || val > result.max)
		result.max= val;

	result.count++ ;
	result.average += val ;
	
	if(index==0)
		result.timestamp= current._timestamp ;

	if(index==array.length-1)
		result.average/=array.length ;

	return result; 
}

function _initReducer() {
	return {
		min: undefined,	
		max: undefined,
		count: 0,	
		average: 0,
		timestamp: 0	
	}
}