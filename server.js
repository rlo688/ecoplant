const express = require('express');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;

const routes = require('./routes')
const config = require('./config');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/', routes);
app.listen(config.server.port, () => {
    console.log(`Server is listening on port ${config.server.port}`);
});

MongoClient.connect(config.db.url, {useUnifiedTopology: true}, (err, client)=>{
	if(err) throw err;
	app.db = client.db(config.db.name);
	console.log("Connected to mongo db")
});

