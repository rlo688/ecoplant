module.exports = {
    db: {
    	name: 'ecoplant',
    	url: 'mongodb://localhost:27017/ecoplant' 
    },
    server: { 
    	port: 3000
    }
}