'use strict';

const Timestamp = require('./timestamp.model') ;

const SampleType = Object.freeze({"VOLUME":"volume", "PRESSURE":"pressure", "TEMPERATURE":"temperature"})

class DataSample {
	/**
	 * @param {SampleType} sampleType sample type
	 * @param {Number} sampleValue sample value
	 * @param {String} timestamp (Optional) valid date string
	 * @param {Number} gmt (Optional) gmt offset
	 */
	constructor(sampleType, sampleValue, timestamp=undefined, gmt=undefined) {
		if(Object.values(SampleType).includes(sampleType)==false) {
			throw `Error creating data sample, Invalid value '${sampleType}' for property 'sampleType'.  property 'sampleType' must be one of following: {${Object.values(SampleType)}}`;
		}

		if(isNaN( Number(sampleValue) )){
			throw `Error creating data sample, Invalid value '${sampleValue}' for property 'sampleValue'.  property 'sampleValue' must be a number` ;
		}

		this._timestamp = Timestamp(timestamp, gmt);
		this._sampleType = sampleType ;
		this._sampleValue = Number(sampleValue) ;
	}
}

module.exports = { DataSample, SampleType } ;