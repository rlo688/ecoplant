'use strict';
const jsep = require("jsep");
const { SampleType } = require('./dataSample.model') ;

class Rule {
	/**
	 * @param {String} expression logical expression
	 * @param {Boolean} debug=false print calculation to console
	 */	
	constructor(expression, debug = false) {
		if(!expression)
			throw `expression cant be undefined` ;
		expression = expression.toLowerCase();
		expression = expression.replace(/[{}]/g,'') ;
		expression = expression.replace(/or/g,'||') ;
		expression = expression.replace(/and/g,'&&') ;
		_validateExpression(expression) ;
		this._expression = expression ;
		this._debug = debug ;
	}

	static createFromJson(ruleObj) {
		return new this(ruleObj._expression);
	}

	evaluate(latestSamples, debug=this._debug) {
		let expressionAfterAssignment = this._expression ;
		Object.values(SampleType).forEach( (sampleType)=> {
			if(isNaN(latestSamples[sampleType])) {
				throw `Error in Rule.evaluate Invalid param '${sampleType}'; either missing or value is not a number. latestSamples=${latestSamples}'`;
			}
			expressionAfterAssignment = expressionAfterAssignment.replace( new RegExp(sampleType, "g") , latestSamples[sampleType]);
		});
		const result = eval ( expressionAfterAssignment );
		if(debug) {
			console.log('Evaluating Rule', '\x1b[35m', this._expression ,'\x1b[0m', 'width data', '\x1b[32m', JSON.stringify(latestSamples), '\x1b[0m',  "Result=", '\x1b[31m', result ,'\x1b[0m',);
			console.log("expressionAfterAssignment: "+expressionAfterAssignment);
		}
		return result;
	}
}

function _validateExpression(expression,debug=false) {
	const tree = jsep(expression) ;
	_validateExpressionNode(tree) ;
}

function _validateExpressionNode(node) {
	switch(node.type) {
		case 'Literal':
			if(isNaN(node.value))
				throw `Only numeric literals allowed; '${node.value}' is not a number`;
			break ; 
		case 'LogicalExpression':
		case 'BinaryExpression':
			_validateExpressionNode(node.left);
			_validateExpressionNode(node.right);
			break;
		case 'Identifier':
			if(Object.values(SampleType).includes(node.name)==false)
				throw `Unknown Identifier '${node.name}'; Only following Identifiers allowed: ${Object.values(SampleType)}` ;
			break;
		case 'Compound':
			throw `Failed to parse boolean expression.  Unknown Compound '${node.body[1].name}'?`
	}
}

module.exports = { Rule } ;