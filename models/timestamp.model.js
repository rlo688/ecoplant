'use strict';

module.exports = function(timestamp = undefined, gmt = undefined) {
	let _timestamp = new Date() ;
	if(timestamp) {
		_timestamp = new Date(timestamp);
		_validateDate(_timestamp);
		if(gmt!=undefined)
		{
			_timestamp.convertFromGmt(gmt);
		}
	}
	return _timestamp ;
}

function _validateDate(dateObj) {
	if (!(Object.prototype.toString.call(dateObj) === "[object Date]")) {
		throw `Error creating date object.  'timestamp' must be valid date` ; //not a date object
	}
	if (isNaN(dateObj.getTime())) {
		throw `Error creating date object, Invalid value for property 'timestamp'.  must be valid date` ; // date is not valid
	}
}

Date.prototype.addHours = function(h) {
  this.setTime(this.getTime() + (h*60*60*1000));
  return this;
}

Date.prototype.convertFromGmt =function(gmtOffset) {
	this.addHours( -1 * (this.getTimezoneOffset()/60) ) ;
	this.addHours( -1 * gmtOffset ) ;
}