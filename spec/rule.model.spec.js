
const { Rule } = require('../models/rule.model') ;

describe("Rule", function() {
	it("throw error for invalid expression", function() {
		const createInvalid = () => new Rule("Temperature > 100 blablabla Temperature < 100"); 
		expect(createInvalid).toThrow();
	});

	it("create rule with valid data", function() {
		let rule ;
		const createRule = () => rule = new Rule("Temperature > 100 and Temperature < 1000"); 
		expect(createRule).not.toThrow();
	});

	it("throw error for invalid identifier", function() {
		let rule ;
		const createRule = () => rule = new Rule("Temperature > 100 and Spins < 1000");
		expect(createRule).toThrow();
	});

	it("Evaluate boolean expressions", function(){
		let latestSamples = { pressure: 0, temperature: 0, volume: 0} ;

		let rule = new Rule("10 > 100 or 5 < 6");
		let result = rule.evaluate(latestSamples);
		expect(result).toBe(true);

		rule = new Rule("10 > 100 and 5 < 6");
		result = rule.evaluate(latestSamples);
		expect(result).toBe(false);
	});

	it("Evaluate expression with data samples", function(){
		let rule = new Rule("{temperature} < 50") ;
		let latestSamples = { pressure: 0, temperature: 0, volume: 0} ;
		while(latestSamples.temperature<100) {
			let result = rule.evaluate(latestSamples) ;
			if(latestSamples.temperature<50)
				expect(result).toBe(true);
			else
				expect(result).toBe(false);
			latestSamples.temperature+=10 ;
		}
	});
});