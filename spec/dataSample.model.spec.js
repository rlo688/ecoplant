
const { DataSample , SampleType } = require('../models/dataSample.model') ;

describe("DataSample", function() {
	it("throw error for invalid data", function() {
		const createInvalidDataSample = () => new DataSample("volumezzz", "1.2345"); 
		expect(createInvalidDataSample).toThrow();
	});

	it("created with valid data", function() {
		let dataSample ;
		const createDataSample = () => dataSample = new DataSample("volume", "1.2345"); 
		expect(createDataSample).not.toThrow();
		expect(dataSample._sampleValue).toBe(1.2345);
	});

	it("detect invalid date", function() {
		const createInvalidDataSample = () => new DataSample("volume", "1.2345","INVALID DATE"); 
		expect(createInvalidDataSample).toThrow();
	});  

	it("support different timezones", function() {
		let jeruslamTimeSample = new DataSample(SampleType.VOLUME, "2.2", "February 26, 2020 15:15:30",2);
		let newYorkTimeSample = new DataSample(SampleType.VOLUME, "1.33", "February 26, 2020 8:15:30",-5);
		expect(jeruslamTimeSample._timestamp.getTime()).toEqual(newYorkTimeSample._timestamp.getTime());

		jeruslamTimeSample = new DataSample(SampleType.VOLUME, "2.2", "February 26, 2020 15:15:30",2);
		newYorkTimeSample = new DataSample(SampleType.VOLUME, "1.33", "February 26, 2020 8:15:31",-5);
		expect(jeruslamTimeSample._timestamp.getTime()).toBeLessThan(newYorkTimeSample._timestamp.getTime());
	});
});