# Ecoplant Assessment Assignment #
## Time Series API Service  ##

Home test for ecoplant [assignment](https://docs.google.com/document/d/1jV9PYapOMOco34jKT8iyNJM-i-Gq2-7OhC-WSpRjedM)

### Install ###
```
git clone https://bitbucket.org/rlo688/ecoplant.git
npm install
```

### Run ###
```
npm start
```

### Run tests ###
```
npm run test
```

*****************************************

author: Harel Ilan, ilan.harel2@gmail.com

*****************************************